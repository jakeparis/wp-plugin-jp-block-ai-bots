<?php
/*
Plugin Name: JP Block AI Bots
Plugin URI:  https://gitlab.com/jakeparis/jp-block-ai-bots
Description: Block ChatGPT from scraping your site to use in their training sets.
Version:     1.0.0
Requires at least: 6.2
Tested up to: 6.3
Author:      Jake Paris
Author URI:  https://jakeparis.com/
License:     GPL-3.0
License URI: https://opensource.org/licenses/GPL-3.0
*/

define('JP_BLOCK_AI_PLUGIN_VERSION', '1.0.0');


/**
 * @see https://platform.openai.com/docs/gptbot
 */
add_filter( 'robots_txt', function($output){
	$blockChatGPT = <<<GPT
User-agent: GPTBot
Disallow: /
GPT;

	return $blockChatGPT . "\n\n" . $output;
});


/**
 * Updater
 */
require plugin_dir_path(__FILE__) . 'update-checker/plugin-update-checker.php';
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;

$myUpdateChecker = PucFactory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/wp-plugin-jp-block-ai-bots',
	__FILE__,
	'jp-block-ai-bots/jp-block-ai-bots.php'
);



